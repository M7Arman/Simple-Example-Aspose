package org.aspose.attempts;

//import com.aspose.words.*;

import com.aspose.words.*;

/**
 * Aspose example main class
 *
 */
public class AsposeMain
{
    public static void main( String[] args ) throws Exception {

        Document doc = new Document();
        // DocumentBuilder provides members to easily add content to a document.
        DocumentBuilder builder = new DocumentBuilder(doc);

        Font font = builder.getFont();
        font.setSize(16);
        font.setColor(java.awt.Color.BLUE);
        font.setName("Arial");

         builder.insertParagraph();
        // builder.insertParagraph();

        builder.startTable();
        builder.insertCell();

        // Set height and define the height rule for the header row.
       // builder.getRowFormat().setHeight(40.0);
       // builder.getRowFormat().setHeightRule(HeightRule.AT_LEAST);

        // Some special features for the header row.
        builder.getCellFormat()
                .getShading()
                .setBackgroundPatternColor(
                        new java.awt.Color(198, 217, 241));
        builder.getParagraphFormat()
                .setAlignment(ParagraphAlignment.CENTER);
        builder.getFont().setSize(16);
        builder.getFont().setName("Arial");
        builder.getFont().setBold(true);

        builder.getCellFormat().setWidth(100.0);
        builder.write("First Name");
        builder.insertCell();
        builder.write("Last Name");
        builder.insertCell();
        builder.write("Address");
        builder.insertCell();
        builder.write("City");
        builder.insertCell();
        builder.write("Telephone");
        builder.endRow();
        // Set features for the other rows and cells.
        builder.getCellFormat().getShading()
                .setBackgroundPatternColor(java.awt.Color.WHITE);
        builder.getCellFormat().setWidth(100.0);
        builder.getCellFormat().setVerticalAlignment(
                CellVerticalAlignment.CENTER);

        doc.save("output.docx", SaveFormat.DOCX);
    }
}
